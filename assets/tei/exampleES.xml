<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://vault.tei-c.org/P5/current/xml/tei/custom/schema/relaxng/tei_simplePrint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Descripción de Buenos Aires</title>
        <author ref="http://id.worldcat.org/fast/1803291/">Acarete du Biscay (s. XVII)</author>
        <editor>Equipo SOFI4</editor>
        <editor>Iñaki Cano García #ICG</editor>
        <respStmt>
         <resp>Instructores</resp>
         <name>Raffaele Viglianti</name>
         <name>Gimena del Río Rande</name>
         <name>Nidia Hernández</name>
         <name>Romina de León</name>
       </respStmt>
      </titleStmt>
      <publicationStmt>
        <p>
        <ref target="https://creativecommons.org/licenses/by-nc-sa/4.0/">
        Released under a Creative Commons BY-NC-SA License
       </ref>
       </p>
      </publicationStmt>
      <sourceDesc>
        <bibl>
          <pubPlace>Buenos Aires</pubPlace>
          <publisher>La Revista de Buenos Aires</publisher>
          <traductor>Daniel Maxwell</traductor>
          <date when="1867-05">Mayo de 1867</date>
        </bibl>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>
        Proyecto del curso "Publicaciones Digitales Mínimas", University of Maryland (EE.UU.) / Universidad del Salvador /
        CONICET (Argentina), primavera austral de <date when="2020">2020</date>.
        </p>
      </projectDesc>
      <editorialDecl><!--<hypenation> none </hyphenation> don't work under TEI SimplePrint?-->
        <p>
        Nuestra codificación refleja todas las peculiaridades gráficas del original salvo la separación silábica con guiones al final de línea.
        </p>
      </editorialDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="es">Español</language>
      </langUsage>
      <textClass>
        <keywords scheme="FAST">
          <term ref="http://id.worldcat.org/fast/1155691/">Travelers</term>
          <term ref="http://id.worldcat.org/fast/1155743/">Travelers' writings, French</term>
          <term ref="http://id.worldcat.org/fast/1205786/">Argentina--Buenos Aires</term>
        </keywords>
      </textClass>
    </profileDesc>
  </teiHeader>

  <text xml:lang="es">
    <body>
      <div>
          <pb n="16"/>
          <head type="subtitle">
          <choice><orig>DESCRIPCION</orig><reg>DESCRIPCIÓN</reg></choice> DE <name type="place">BUENOS AIRES</name>
          </head>
        <p n="01">
          <lb/>Antes de decir nada de mi viaje al <name type="place">Perú</name><!--He marcado Perú las tres veces como place y no como country porque
          el Perú del siglo XVII no corresponde con el país actual #ICG-->, anotaré lo que
          <lb/>observé de remarcable en <name type="place">Buenos Aires</name> mientras permanecí
          <lb/>allí. El aire es bastante templado, muy semejante al de
          <lb/><name type="place"><choice><sic>Andaluía</sic><corr>Andalucía</corr></choice></name>,
            pero no tan caliente: las lluvias caen casi con
          <lb/>tanta frecuencia en el verano como en el invierno; y la lluvia
          <lb/>en los tiempos de bochorno, <choice><sic>frecuentemeate</sic><corr>frecuentemente</corr></choice> produce diversas
          <lb/>clases de sapos, que son muy comunes en estos países,
            <fw type="gathering">2</fw>
            <pb n="17"/>
          <lb/>
            <fw type="head_odd">UN LIBRO CURIOSO Y RARO.</fw>
            <fw type="pagenumber_odd">17</fw>
          <lb/>pero no ponzoñosos. El pueblo está situado en un terreno elevado
          <lb/><choice><orig>á</orig><reg>a</reg></choice> orillas del <name type="placeGeog"><choice><orig>Rio</orig><reg>Río</reg></choice> de la Plata</name>,
            <choice><orig>á</orig><reg>a</reg></choice> tiro de fusil del canal,
          <lb/>en un ángulo de tierra formado por un pequeño riacho
          <lb/>llamado <name type="placeGeog">Riochuelo</name> <ref target="#n1_p17" xml:id="r1_p17">(1)</ref> que desagua en el
            <choice><orig>rio</orig><reg>río</reg></choice>
            <choice><orig>á</orig><reg>a</reg></choice> un cuarto de
          <lb/>legua del pueblo; contiene cuatrocientas casas y no tiene cerco,
          <lb/>ni muro, ni foso, y nada que lo defienda, sino un pequeño
          <lb/>fuerte de tierra que domina el
            <choice><orig>rio</orig><reg>río</reg></choice>, circundado por un
          <lb/>foso y monta diez cañones de <!--modernizamos "hierro"?--><orig>fierro</orig>, siendo el de mayor calibre
          <lb/>de
            <choice><orig>á</orig><reg>a</reg></choice> doce. Allí reside el <name type="personRoleName">Gobernador</name> y la
            <choice><orig>guarnicion</orig><reg>guarnición</reg></choice>
          <lb/>se compone de solo 150 <rs type="people">hombres</rs> divididos en tres
            <choice><orig>compañias</orig><reg>compañías</reg></choice>,
          <lb/>mandadas por tres <rs type="people">capitanes</rs> nombrados por este
            <choice><orig>á</orig><reg>a</reg></choice> su
          <lb/>antojo, y
            <choice><orig>á</orig><reg>a</reg></choice> quienes cambia con tanta frecuencia, que apenas
          <lb/>hay un <rs type="people">ciudadano rico</rs> que no haya sido <rs type="people">capitán</rs>. Estas
          <lb/><choice><orig>compañias</orig><reg>compañías</reg></choice> no siempre están completas, porque los <rs type="people">soldados</rs>,
          <lb/>inducidos por la baratura con que se vive en aquellos
            <choice><orig>paises</orig><reg>países</reg></choice>,
          <lb/>frecuentemente desertan,
            <choice><sic>apesar</sic><corr>a pesar</corr></choice> de los esfuerzos que
          <lb/>se
            <choice><sic>hacepor</sic><corr>hace por</corr></choice> retenerlos en el servicio pagándoles altos sueldos,
          <lb/>que llegan
            <choice><orig>á</orig><reg>a</reg></choice> cuatro reales diarios, que equivale
            <choice><orig>á</orig><reg>a</reg></choice> un
          <lb/>chelín y seis peniques moneda inglesa, y un pan de tres peniques,
          <lb/>que es cuanto puede comer un <rs type="people">hombre</rs>. Pero el <name type="personRoleName">Gobernador</name>
          <lb/>conserva en una llanura inmediata como 1200 caballos
          <lb/>mansos para su servicio ordinario, y en caso de necesidad
          <lb/>para hacer montar
            <choice><orig>á</orig><reg>a</reg></choice> los <rs type="people">habitantes del pueblo</rs>, formando
          <lb/>así un pequeño cuerpo de
            <choice><orig>caballeria</orig><reg>caballería</reg></choice>.
        </p>
        <note xml:id="n1_p17">
          <lb/><ref target="#r1_p17">1.</ref> Casi todas las cartas geográficas y <rs type="people">viajeros
            <choice><orig>estranjeros</orig><reg>extranjeros</reg></choice></rs> al
          <lb/>habla española, desconociendo la
            <choice><orig>raiz</orig><reg>raíz</reg></choice> del nombre de este arroyo
          <lb/>lo escriben siempre como se
            <choice><orig>vé</orig><reg>ve</reg></choice> aquí. (
            <choice><abbr>N.</abbr><expan>Nota</expan></choice> del
            <rs type="people"><choice><abbr>T.</abbr><expan>traductor</expan></choice></rs>)
        </note>
        <p n="02">
          <lb/>Además de este fuerte hay un pequeño
            <choice><sic>buluarte</sic><corr>baluarte</corr></choice> en la
          <lb/><name type="place">Boca del Riachuelo</name>, donde existe una guardia; monta dos
          <lb/>pequeños cañones de <orig>fierro</orig><!--fierro?-->, de
            <choice><orig>á</orig><reg>a</reg></choice> tres. Este baluarte domina
          <lb/>el punto donde atracan las lanchas para descargar
            <choice><orig>ó</orig><reg>o</reg></choice> recibir
          <lb/>
            <fw type="gathering">2</fw>
          <pb n="18"/>
          <lb/>
            <fw type="pagenumber_even">18</fw>
            <fw type="head_even">LA REVISTA DE BUENOS AIRES.</fw>
          <lb/>efectos, estando estas sujetas
            <choice><orig>á</orig><reg>a</reg></choice> ser visitadas por los <rs type="people">oficiales</rs>
          <lb/>del baluarte cuando están descargando
            <choice><orig>ó</orig><reg>o</reg></choice> cargando.
        </p>
        <p n="03">
          <lb/>Las casas del pueblo son construidas de barro, porque
          <lb/>hay poca piedra en todos estos
            <choice><orig>paises</orig><reg>países</reg></choice> hasta llegar al <name type="place">Perú</name>;
          <lb/>están techadas con cañas y paja y no tienen altos; todas las
          <lb/>piezas son de un solo piso y muy espaciosas; tienen grandes
          <lb/>patios, y detrás de las casas grandes huertas, llenas de naranjos,
          <lb/>limoneros, higueras, manzanos, peros y otros árboles
          <lb/>frutales, con legumbres en abundancia, como coles, cebollas,
          <lb/>ajos, lechuga, <orig>alberjas</orig>
<!--Marcamos la forma "alberja" como <sic> o como <orig> o de ninguna forma?--> y habas; sus melones especialmente
          <lb/>son
            <choice><orig>escelentes</orig><reg>excelentes</reg></choice>, pues la tierra es muy fértil y buena;
          <lb/>viven muy cómodamente y
            <choice><orig>á</orig><reg>a</reg></choice>
            <choice><orig>escepcion</orig><reg>excepción</reg></choice> del vino que es
          <lb/>algo caro, tienen toda clase de alimentos en abundancia, como
          <lb/>carne de vaca y ternera, de carnero y de venado, liebre,
          <lb/>gallinas, patos,
            <choice><sic>ganzos</sic><corr>gansos</corr></choice> silvestres, perdices, pichones, tortugas
          <lb/>y aves de caza de toda especie, y tan baratas que pueden
          <lb/>comprarse perdices
            <choice><orig>á</orig><reg>a</reg></choice> un penique cada una y lo demás en
          <lb/>
            <choice><orig>proporcion</orig><reg>proporción</reg></choice>.
        </p>
        <p n="04">
          <lb/>Hay
            <choice><orig>tambien</orig><reg>también</reg></choice> numerosos avestruces <!--ñandús!!!-->que andan en tropillas
          <lb/>como el ganado, y aún cuando su carne es buena, nadie
          <lb/>sino los <rs type="people">salvajes</rs> come de ella. Hacen paraguas<!--la versión francesa dice parasol--> de sus
          <lb/>plumas, que son muy cómodos para el sol; sus huevos son
          <lb/>buenos y <rs type="people">todos</rs> comen de ellos, aun cuando se dice que son
          <lb/>indigestos. Observé en estos animales una cosa muy notable,
          <lb/>y es esta, que mientras la hembra está echada sobre los
          <lb/>huevos, tienen el instinto de proveer
            <choice><orig>á</orig><reg>a</reg></choice> la
            <choice><orig>mantencion</orig><reg>mantención</reg></choice> de sus
          <lb/>polluelos;
            <choice><sic>asi</sic><corr>así</corr></choice> es que cinco
            <choice><orig>ó</orig><reg>o</reg></choice> seis
            <choice><orig>dias</orig><reg>días</reg></choice> antes de salir estos de
          <lb/>la cáscara, colocan un huevo en cada uno de los cuatro
            <choice><orig>estremos</orig><reg>extremos</reg></choice>
          <lb/>del lugar en donde están echados, y quebrándolos,
          <lb/>procréanse en estos moscas y gusanos en gran número que
          <lb/>sirven para alimentar
            <choice><orig>á</orig><reg>a</reg></choice> los pequeños avestruces desde el
          <pb n="19"/>
          <lb/>
            <fw type ="head_odd">UN LIBRO CURIOSO Y RARO.</fw>
            <fw type="pagenumber_odd">19</fw>
          <lb/>tiempo en que salen de la cáscara, hasta que se hallan en
          <lb/>aptitud de ir
            <choice><orig>mas</orig><reg>más</reg></choice> lejos en busca de alimentos.
        </p>
        <p n="05">
          <lb/>Las casas de los <rs type="people">habitantes de primera clase</rs> están adornadas
          <lb/>con colgaduras, cuadros y otros ornamentos y muebles
          <lb/>decentes, y todos los que se encuentran en situación <!--con tilde??--> regular
          <lb/>son servidos en vajilla de plata y tienen muchos <rs type="people">sirvientes</rs>,
          <lb/>
            <rs type="people">negros</rs>, <rs type="people">mulatos</rs>, <rs type="people">mestizos</rs>,
            <rs type="people">indios</rs>, <rs type="people">cafres</rs>
            <choice><orig>ó</orig><reg>o</reg></choice> <rs type="people">zambos</rs>, siendo
          <lb/>todos estos <rs type="people">esclavos</rs>. Los <rs type="people">negros</rs> proceden de <name type="place">Guinea</name>, los
          <lb/><rs type="people">mulatos</rs> son el engendro de un <rs type="people">español</rs>, en una <rs type="people">negra</rs>, los
          <lb/>
            <rs type="people">mestizos</rs> son el fruto de una <rs type="people">india</rs> y un
            <rs type="people">español</rs>, y los <rs type="people">zambos</rs>
          <lb/>de un <rs type="people">indio</rs> y una <rs type="people">mestiza</rs>, distinguibles todos por el color
          <lb/>de su tez y su pelo.
        </p>
        <p n="06">
          <lb/>Estos <rs type="people">esclavos</rs> son empleados en las casas de sus <rs type="people">amos</rs>
            <choice><orig>ó</orig><reg>o</reg></choice>
          <lb/>en cultivar
            <choice><sic>su</sic><corr>sus</corr></choice> terrenos, pues tienen grandes chacras abundantemente
          <lb/>sembradas de granos, como trigo, cebada y mijo;
          <lb/>
            <choice><orig>ó</orig><reg>o</reg></choice> bien para cuidar de sus caballos,
            <choice><orig>ó</orig><reg>o</reg></choice> mulas, que en todo el
          <lb/>año solo se alimentan con pasto
            <choice><orig>ó</orig><reg>o</reg></choice> bien en matar toros cerriles,
          <lb/>y finalmente para cualquier otro servicio.
        </p>
        <p n="07">
          <lb/>Toda la riqueza de estos <rs type="people">habitantes</rs> consiste en ganados
          <lb/>que se multiplican tan prodigiosamente en <!--marcar esto como place??-->esta provincia,
          <lb/>que las llanuras están cubiertas de ellos particularmente de
          <lb/>toros, vacas, ovejas, caballos, yeguas, mulas, burros, cerdos,
          <lb/>venados y otros, de tal modo, que si no fuese por un número
          <lb/>de perros que se devoran los terneros y otros animales
          <lb/>tiernos,
            <choice><orig>devastarian</orig><reg>devastarían</reg></choice> el
            <choice><orig>pais</orig><reg>país</reg></choice>. Sacan tanto provecho de las
          <lb/>pieles y cueros de estos animales, que un solo ejemplo bastará
          <lb/>para dar una idea de
            <choice><orig>cuanto</orig><reg>cuánto</reg></choice>
            <choice><orig>podria</orig><reg>podría</reg></choice> este aumentarse
          <lb/>en buenas manos.
        </p>
<!--Here starts the second half of the Spanish text. Aquí empieza la segunda mitad del texto en español-->
        <p n="08">
          <lb/>Los
            <choice><orig>veintidos</orig><reg>veintidós</reg></choice> buques holandeses que encontramos en
          <lb/>Buenos Aires
            <choice><orig>á</orig><reg>a</reg></choice> nuestra llegada, estaban cargados, cada uno
          <lb/>de
            <choice><sic>elloscon</sic><corr>ellos con</corr></choice> 13
            <choice><orig>á</orig><reg>a</reg></choice> 14,000 cueros de toro cuando menos, cuyo
          <pb n="20"/>
          <lb/>
            <fw type="pagenumber_even">20</fw>
            <fw type="head_even">LA REVISTA DE BUENOS AIRES.</fw>
          <lb/>valor asciende
            <choice><orig>á</orig><reg>a</reg></choice> 300,000 <foreign xml:lang="fr">livers</foreign>
            <choice><orig>ó</orig><reg>o</reg></choice> sean 33,500 libras esterlinas,
          <lb/>comprados como lo fueron por los <rs type="people">holandeses</rs>
            <choice><orig>á</orig><reg>a</reg></choice>
          <lb/>siete
            <choice><orig>ú</orig><reg>u</reg></choice> ocho reales cada uno; es decir,
            <choice><orig>á</orig><reg>a</reg></choice> menos de una corona
          <lb/><ref target="#n1_p20" xml:id="r1_p20">(1)</ref> inglesa, los que fueron vendidos
            <choice><orig>despues</orig><reg>después</reg></choice> en <name type="place">Europa</name>
            <choice><orig>á</orig><reg>a</reg></choice> 25 chelines ingleses, cuando menos.
        </p>
        <note>
          <lb/><ref target="#r1_p20" xml:id="n1_p20">1.</ref> La corona inglesa vale 5 chelines (
            <choice><abbr>N.</abbr><expan>Nota</expan></choice> del
            <rs type="people">
              <choice><abbr>T.</abbr><expan>traductor</expan></choice>
            </rs>)
        </note>
        <p n="09">
          <lb/>Cuando yo manifesté mi asombro al ver tan infinito número
          <lb/>de animales, me refirieron una estratagema de que
          <lb/>se valen
            <choice><orig>á</orig><reg>a</reg></choice> veces cuando temen el desembarque de <rs type="people">enemigos</rs>,
          <lb/>que
            <choice><orig>tambien</orig><reg>también</reg></choice> es asunto de maravillarse y es como sigue: arrean
          <lb/>tal enjambre de toros, vacas, caballos y otros animales
            <choice><orig>á</orig><reg>a</reg></choice> la costa del
            <choice><orig>rio</orig><reg>río</reg></choice>, que es absolutamente imposible
            <choice><orig>á</orig><reg>a</reg></choice> cualquier
          <lb/>número de <rs type="people">hombres</rs>, aun cuando no temiesen la furia
          <lb/>de estos animales bravíos, el hacerse camino por en medio
          <lb/>de un tropa tan inmensa de bestias.
        </p>
        <p n="10">
          <lb/>Los primeros <rs type="people">habitantes</rs> de este pueblo pusiéronles cada
          <lb/>uno su marca
            <choice><orig>á</orig><reg>a</reg></choice> todos los que pudieron tomar echándolos
          <lb/>
            <choice><orig>despues</orig><reg>después</reg></choice> dentro de su cercas; pero multiplícanse tan rápidamente
          <lb/>que viéronse luego obligados
            <choice><orig>á</orig><reg>a</reg></choice> soltarlos, y hoy
          <lb/>van y los matan
            <choice><orig>segun</orig><reg>según</reg></choice> precisan de ellos,
            <choice><orig>ó</orig><reg>o</reg></choice> tienen
            <choice><orig>ocasion</orig><reg>ocasión</reg></choice>
          <lb/>de preparar para venta una cantidad de cueros. Actualmente
          <lb/>solo marcan aquellos caballos y mulas que toman
          <lb/>
            <choice><sic>paraamansar</sic><corr>para amansar</corr></choice> y servirse de ellos. Algunas <rs type="people">personas</rs> hacen
          <lb/>de esto un gran negocio, enviándolos al <name type="place">Perú</name>, donde producen
          <lb/>cincuenta patacones,
            <choice><orig>ó</orig><reg>o</reg></choice> sean 11 libras, 13 chelines y 4
          <lb/>peniques, moneda esterlina, la yunta.
        </p>
        <p n="11">
          <lb/>El mayor número de los <rs type="people">traficantes</rs> en ganados <!--son!!-->están muy
          <lb/>ricos, pero de todos los <rs type="people">negociantes</rs>, los de mas importancia
          <lb/>son los que comercian en
            <choice><orig>mercancias</orig><reg>mercancías</reg></choice> europeas,
          <lb/>reputándose la fortuna de muchos de estos en 2
            <choice><orig>á</orig><reg>a</reg></choice> 300,000
          <pb n="21"/>
          <lb/>
            <fw type="head_odd">UN LIBRO CURIOSO Y RARO.</fw>
            <fw type="pagenumber_odd">21</fw>
          <lb/>coronas
            <choice><orig>ó</orig><reg>o</reg></choice> sean 67,000 libras esterlinas. De modo que el
          <lb/><rs type="people">mercader</rs> que no tiene
            <choice><orig>mas</orig><reg>más</reg></choice> que de 15
            <choice><orig>á</orig><reg>a</reg></choice> 20,000 coronas es
          <lb/>considerado como un mero <rs type="people">vendedor</rs> al menudeo. De estos
          <lb/>últimos hay como 200 <rs type="people">familias</rs> en el pueblo, que hacen
          <lb/>500 <rs type="people">hombres</rs> de armas llevar
            <choice><orig>ademas</orig><reg>además</reg></choice> de sus <rs type="people">esclavos</rs>, que
          <lb/>son el triple de este número, pero que no deben contarse
          <lb/>para la defensa
            <choice><sic>perque</sic><corr>porque</corr></choice> no se les permite cargar armas. <!--aquí acentúa bien-->Así,
          <lb/>pues, los <rs type="people">españoles</rs>, los <rs type="people">portugueses</rs>, los hijos de estos (de
          <lb/>los cuales los que nacen en el <!--quí acentúa bien-->país llámanles <rs type="people">criollos</rs>, para
          <lb/>distinguirlos de los <rs type="people">nativos de <name type="country">España</name></rs>) y algunos <rs type="people">mestizos</rs>
          <lb/>forman la milicia, que, con los <rs type="people">soldados</rs> de la
            <choice><orig>guarnicion</orig><reg>guarnición</reg></choice>,
          <lb/>componen un cuerpo de 600 <rs type="people">hombres</rs>,
            <choice><orig>segun</orig><reg>según</reg></choice> los computé yo
          <lb/>en diversas reuniones, pues tres veces al año, en
            <choice><orig>dias</orig><reg>días</reg></choice> festivos,
          <lb/>forman de parada,
            <choice><orig>á</orig><reg>a</reg></choice> caballo,
            <choice><orig>á</orig><reg>a</reg></choice> inmediaciones del pueblo.
        </p>
        <p n="12">
          <lb/>Observé que entre ellos
            <choice><orig>habia</orig><reg>había</reg></choice> muchos <rs type="people">hombres de
          <lb/>edad</rs> que no llevaban armas de fuego sino solo
            <choice><sic>sí</sic><corr>su</corr></choice> <!--corregido desde la versión inglesa-->espada al
          <lb/>cinto, lanza en la mano y una rodela al hombro. Los
            <choice><orig>mas</orig><reg>más</reg></choice>
          <lb/>de ellos son <rs type="people">hombres casados</rs> y
            <rs type="people"><choice><orig>gefes</orig><reg>jefes</reg></choice> de familia</rs>, y por consiguiente
          <lb/>tienen poca
            <choice><orig>aficion</orig><reg>afición</reg></choice>
            <choice><orig>á</orig><reg>a</reg></choice> los combates. Aman su sosiego
          <lb/>y el placer, y son muy devotos de <rs type="people">Venus</rs>. Confieso
          <lb/>que son hasta cierto punto disculpables
            <choice><orig>á</orig><reg>a</reg></choice> este respecto,
          <lb/>pues las
            <choice><orig>mas</orig><reg>más</reg></choice> de las <rs type="people">mujeres</rs> son
            <choice><orig>estremadamente</orig><reg>extremadamente</reg></choice> bellas,
          <lb/>bien formadas, y de un
            <choice><orig>cútis</orig><reg>cutis</reg></choice> terso; y sin embargo, tan fieles
          <lb/>son
            <choice><orig>á</orig><reg>a</reg></choice> sus <rs type="people">maridos</rs>, que ninguna
            <choice><orig>tentacion</orig><reg>tentación</reg></choice> puede inducirlas
            <choice><orig>á</orig><reg>a</reg></choice> aflojar el nudo sacro; pero, por otra parte, si delinquen
          <lb/>los <rs type="people">maridos</rs>, son
            <choice><orig>á</orig><reg>a</reg></choice> menudo castigados con el veneno
            <choice><orig>ó</orig><reg>o</reg></choice> el
          <lb/>puñal.
        </p>
        <p n="13">
          <lb/>Las <rs type="people">mujeres</rs> son
            <choice><orig>mas</orig><reg>más</reg></choice> numerosas que los <rs type="people">hombres</rs>, y
            <choice><orig>ademas</orig><reg>además</reg></choice>
          <lb/>de <rs type="people">españoles</rs>, hay unos pocos <rs type="people">franceses</rs>, <rs type="people">holandeses</rs> y
          <pb n="22"/>
          <lb/>
            <fw type="pagenumber_even">22</fw>
            <fw type="head_even">LA REVISTA DE BUENOS AIRES.</fw>
          <lb/><rs type="people">genoveses</rs>, pero todos pasan por <rs type="people">españoles</rs>, pues de otro
          <lb/>modo no
            <choice><orig>habria</orig><reg>habría</reg></choice> para ellos cabida allí, y especialmente
          <lb/>para los que en su
            <choice><orig>religion</orig><reg>religión</reg></choice> difieren de los <rs type="people">Católicos Romanos</rs>,
          <lb/>pues allí está establecida la
            <choice><orig>Inquisicion</orig><reg>Inquisición</reg></choice>.
        </p>
        <p n="14">
          <lb/>La renta del <name type="personRoleName">Obispo</name> sube
            <choice><orig>á</orig><reg>a</reg></choice> 3000 patacones,
            <choice><orig>ó</orig><reg>o</reg></choice> sean 700
          <lb/>libras esterlinas anuales. Su diócesis comprende este pueblo
          <lb/>y el de <name type="place">Santa
            <choice><orig>Fé</orig><reg>Fe</reg></choice></name>, con las estancias
            <choice><orig>ó</orig><reg>o</reg></choice> haciendas pertenecientes
            <choice><orig>á</orig><reg>a</reg></choice> ambas. Ocho
            <choice><orig>ó</orig><reg>o</reg></choice> diez <rs type="people">sacerdotes</rs> ofician en
          <lb/>la Catedral, la que, así como las casas particulares, es
          <lb/>construida de barro. Los <rs type="people">Jesuitas</rs> tienen un Colegio; los
          <lb/>
            <rs type="people"><choice><sic>Domínicos</sic><corr>Dominicos</corr></choice></rs>, los <rs type="people">Recoletos</rs> y los <rs type="people">Religiosos de la Merced</rs> tienen
          <lb/>cada uno su convento. Hay
            <choice><orig>tambien</orig><reg>también</reg></choice> un hospital, pero
          <lb/>existe tan poca <rs type="people">gente pobre</rs> en estos
            <choice><orig>paises</orig><reg>países</reg></choice>, que de poco
          <lb/>sirve.
        </p>
      </div>
    </body>
  </text>
</TEI>
